#!/usr/bin/python3

import logging, os, asyncio, sys
logger = logging.getLogger("oszitrace")

from oszitrace.iocapp import OsziIocApplication

def init_ioc(args, env):
    app = OsziIocApplication(args or sys.argv, env or os.environ)
    return app


def run_ioc(args=None, env=None):

    if env is None:
        env = os.environ.copy()

    # try "http://localhost:4317"
    otelcol_url = env.get("OSZI_OTELCOL_URL", "")

    if otelcol_url is not None and len(otelcol_url)>0:
        from opentelemetry._logs import set_logger_provider
        from opentelemetry.exporter.otlp.proto.grpc._log_exporter import OTLPLogExporter
        #  from opentelemetry.sdk._logs.export import ConsoleLogExporter
        from opentelemetry.sdk._logs import LoggerProvider, LoggingHandler
        from opentelemetry.sdk._logs.export import BatchLogRecordProcessor
        from opentelemetry.sdk.resources import Resource

        otel_logger_provider = LoggerProvider(
            resource=Resource.create(
                {
                    "service.name": "oszitrace",
                    "service.instance.id": os.uname().nodename,
                }
            ),
        )
        set_logger_provider(otel_logger_provider)

        otel_otlp_exporter = OTLPLogExporter(endpoint=otelcol_url,
                                             insecure=True)
        otel_logger_provider.add_log_record_processor(BatchLogRecordProcessor(otel_otlp_exporter))

        #  console_exporter = ConsoleLogExporter()
        #  logger_provider.add_log_record_processor(BatchLogRecordProcessor(console_exporter))

        otel_handler = LoggingHandler(level=logging.NOTSET,
                                      logger_provider=otel_logger_provider)

        logger.addHandler(otel_handler)
    else:
        otel_logger_provider = None
        logging.basicConfig()


    logger.setLevel(logging.INFO)
    con = logging.StreamHandler()
    con.setLevel(logging.INFO)
    #formatter = logging.Formatter()
    logger.addHandler(con)

    
    app = init_ioc(args or sys.argv, env or os.environ)
    asyncio.run(app.run())


    if otel_logger_provider is not None:
        otel_logger_provider.shutdown()


if __name__ == "__main__":
    run_ioc(sys.argv, os.environ)
